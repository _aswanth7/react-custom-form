import React, { Component } from "react";
import { validatorFunc } from "../Helper/validator";

class ConfirmPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value ? this.props.value : "",

      error: "",
      confirmError: "",
      hasAnyError: false,
    };
  }

  onChangeHandler = (event) => {
    if (this.props.property.validation) {
      let errors = validatorFunc(
        this.props.property.validation,
        event.target.value,
        this.props.property.type
      );
      let hasAnyError = errors === null ? false : true;
      this.setState({
        value: event.target.value,
        error: hasAnyError ? errors[0] : "",
        hasAnyError,
      });
      this.props.errorHandle(errors);
    } else {
      this.setState({
        value: event.target.value,
      });
    }
  };
  onChangeConfirmHandler = (event) => {
    if (event.target.value !== this.state.value) {
      this.setState({
        confirmError: "Password should match",
      });
    } else {
      this.setState({
        confirmError: "",
      });
    }
  };
  render() {
    let property = this.props.property;
    let err = "";
    if (this.state.error) {
      err = this.state.error;
    }

    let cError = "";
    if (this.state.confirmError) {
      cError = this.state.confirmError;
    }
    return (
      <div>
        <div key={property.id} className="form-group">
          {property.labelname ? (
            <label htmlFor={property.id}>{property.labelname}</label>
          ) : null}
          <input
            type="password"
            name={property.name}
            value={this.state.value}
            className="form-control"
            id={property.id}
            aria-describedby={`${property.type}help`}
            placeholder={property.placeholder ? property.placeholder : null}
            onChange={this.onChangeHandler}
            onBlur={this.onChangeHandler}
            required={
              property.validation && property.validation.includes("required")
                ? true
                : false
            }
          />

          <div className="text-danger">{err}</div>
          {property.validation && property.validation.includes("password") ? (
            <small id="passwordHelpBlock" className="form-text text-muted">
              Your password must be 8 characters long, contain letters and
              numbers aleast 1 symbol , uppercase, lowercase.
            </small>
          ) : null}
        </div>
        <div key={`c-${property.id}`} className="form-group">
          <input
            type="password"
            name={`cpass-${property.name}`}
            value={this.state.confirmValue}
            className="form-control"
            id={`cpass-${property.id}`}
            aria-describedby={`${property.type}help`}
            placeholder="Confirm password"
            onChange={this.onChangeConfirmHandler}
            onBlur={this.onChangeConfirmHandler}
            required={
              property.validation && property.validation.includes("required")
                ? true
                : false
            }
          />

          <div className="text-danger">{cError}</div>
        </div>
      </div>
    );
  }
}

export default ConfirmPassword;
