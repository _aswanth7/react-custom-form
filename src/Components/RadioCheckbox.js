import React from "react";

class RadioCheckbox extends React.Component {
  render() {
    let property = this.props.property;
    return (
      <div className="form-group">
        <div className={`custom-control custom-${property.type}`}>
          <input
            type={property.type}
            id={property.id}
            name={property.name}
            value={property.value}
            className="custom-control-input"
            required={
              property.validation && property.validation.includes("required")
                ? true
                : false
            }
          />
          {property.labelname ? (
            <label className="custom-control-label" htmlFor={property.id}>
              {property.labelname}
            </label>
          ) : null}
        </div>
      </div>
    );
  }
}

export default RadioCheckbox;
