import React from "react";

class SubmitTag extends React.Component {
  render() {
    let property = this.props.property;

    return (
      <div key={property.id} className="form-group">
        <input
          type={property.type}
          name={property.name}
          value={property.value ? property.value : property.type}
          className="btn btn-primary"
          id={property.id}
        />
      </div>
    );
  }
}

export default SubmitTag;
