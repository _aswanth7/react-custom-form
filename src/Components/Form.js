import React, { Component } from "react";
import ConfirmPassword from "./ConfirmPassword";
import Textarea from "./Textarea";
import InputTag from "./InputTag";
import Dropdown from "./Dropdown";
import SubmitTag from "./SubmitTag";
import RadioCheckbox from "./RadioCheckbox";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    };
  }
  createForm = (property) => {
    const formElement = [];

    switch (property.type) {
      case "text":
      case "email":
      case "number":
      case "password":
        formElement.push(
          <InputTag
            key={property.id}
            property={property}
            errorHandle={this.changeErrorState}
          />
        );
        break;
      case "cpassword":
        formElement.push(
          <ConfirmPassword
            key={property.id}
            property={property}
            errorHandle={this.changeErrorState}
          />
        );
        break;
      case "radio":
      case "checkbox":
        formElement.push(
          <RadioCheckbox
            key={property.id}
            property={property}
            errorHandle={this.changeErrorState}
          />
        );
        break;
      case "select":
        formElement.push(
          <Dropdown
            key={property.id}
            property={property}
            errorHandle={this.changeErrorState}
          />
        );
        break;
      case "textarea":
        formElement.push(
          <Textarea
            key={property.id}
            property={property}
            errorHandle={this.changeErrorState}
          />
        );
        break;
      case "submit":
        formElement.push(
          <SubmitTag
            key={property.id}
            property={property}
            errorHandle={this.changeErrorState}
          />
        );
        break;

      default:
        console.error("Not a valid html type.");
        break;
    }
    return formElement;
  };

  changeErrorState = (errors) => {
    if (errors != null && errors.length > 0) {
      this.setState({
        hasError: true,
      });
    } else {
      this.setState({
        hasError: false,
      });
    }
  };

  onSubmit = (event) => {
    event.preventDefault();

    let data = {};
    if (!this.state.hasError) {
      for (let index = 0; index < event.target.length; index++) {
        data[event.target[index].name] = event.target[index].value;
      }
      this.props.callback(data);
    } else {
      console.log("error");
    }
  };
  render() {
    return (
      <form onSubmit={this.onSubmit}>
        {this.props.property.map(this.createForm).map((tag) => {
          return tag;
        })}
      </form>
    );
  }
}

export default Form;
