import React, { Component } from "react";
import { validatorFunc } from "../Helper/validator";

class InputTag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      hasAnyError: false,
    };
    this.inputTag = React.createRef();
  }
  componentDidMount() {
    if (this.props.property.value) {
      this.inputTag.current.value = this.props.property.value;
    }
  }

  onChangeHandler = (event) => {
    console.log(this.inputTag.current.value, "current");
    if (this.props.property.validation) {
      let errors = validatorFunc(
        this.props.property.validation,
        event.target.value,
        this.props.property.type
      );
      let hasAnyError = errors === null ? false : true;
      this.setState({
        error: hasAnyError ? errors[0] : "",
        hasAnyError,
      });
      //this.inputTag.current.va
      this.props.errorHandle(errors);
    }
  };

  render() {
    let property = this.props.property;
    let err = "";
    if (this.state.error) {
      console.log(this.state.error, "state");
      err = this.state.error;
    }
    return (
      <div key={property.id} className="form-group">
        {property.labelname ? (
          <label htmlFor={property.id}>{property.labelname}</label>
        ) : null}
        <input
          ref={this.inputTag}
          type={property.type}
          name={property.name}
          className="form-control"
          id={property.id}
          aria-describedby={`${property.type}help`}
          placeholder={property.placeholder ? property.placeholder : null}
          onChange={this.onChangeHandler}
          onBlur={this.onChangeHandler}
          required={
            property.validation && property.validation.includes("required")
              ? true
              : false
          }
        />

        <div className="text-danger">{err}</div>
      </div>
    );
  }
}

export default InputTag;
