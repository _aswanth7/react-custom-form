import React from "react";
import { validatorFunc } from "../Helper/validator";

class Textarea extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      hasAnyError: false,
    };
    this.inputTag = React.createRef();
  }
  componentDidMount() {
    if (this.props.property.value) {
      this.inputTag.current.value = this.props.property.value;
    }
  }
  onChangeHandler = (event) => {
    if (this.props.property.validation) {
      let errors = validatorFunc(
        this.props.property.validation,
        event.target.value,
        this.props.property.type
      );
      let hasAnyError = errors === null ? false : true;

      this.setState({
        error: hasAnyError ? errors[0] : "",
        hasAnyError,
      });
      this.props.errorHandle(errors);
    }
  };
  render() {
    let property = this.props.property;
    let err = "";

    if (this.state.error) {
      err = this.state.error;
    }
    return (
      <div className="form-group">
        {property.labelname ? (
          <label htmlFor={property.id}>{property.labelname}</label>
        ) : null}
        <textarea
          name={property.name}
          className="form-control"
          ref={this.inputTag}
          id={property.id}
          placeholder={property.placeholder ? property.placeholder : null}
          rows={property.row ? property.row : 3}
          onChange={this.onChangeHandler}
          onBlur={this.onChangeHandler}
          required={
            property.validation && property.validation.includes("required")
              ? true
              : false
          }
        ></textarea>
        <div className="text-danger">{err}</div>
      </div>
    );
  }
}

export default Textarea;
