import React from "react";

class Dropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "",
    };
  }

  onChangeHandler = (event) => {
    // console.log(event.target.value);

    this.setState({
      selected: event.target.value,
    });
  };
  render() {
    let property = this.props.property;
    return (
      <div className="form-group">
        {property.labelname ? (
          <label htmlFor={property.id}>{property.labelname}</label>
        ) : null}
        <select
          name={property.name}
          id={property.id}
          onChange={this.onChangeHandler}
          onClick={this.onChangeHandler}
          className="custom-select"
        >
          {property.options.map((options, index) => {
            return (
              <option key={index} value={options.value}>
                {options.text}
              </option>
            );
          })}
        </select>
        {property.invalid ? (
          <div class="invalid-feedback">{property.invalid}</div>
        ) : null}
      </div>
    );
  }
}

export default Dropdown;
