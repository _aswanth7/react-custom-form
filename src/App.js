import React from "react";
import "./App.css";
import Form from "./Components/Form";

class App extends React.Component {
  onchange = (event) => {
    console.log(event);
  };

  property = [
    {
      type: "text",
      name: "name",
      id: "name",
      value: "aswanth",
      placeholder: "Enter Your name",
      labelname: "Enter Your name",
      validation: ["required", "min:3", "max:8"],
    },
    {
      type: "email",
      name: "email",
      id: "email",
      placeholder: "Enter Your mail",
      labelname: "Enter Your mail",
      validation: ["required", "email"],
    },
    {
      type: "cpassword",
      name: "password",
      id: "password",
      placeholder: "Enter Your password",
      labelname: "Enter Your password",
      validation: ["required", "password"],
    },
    {
      type: "number",
      name: "number",
      id: "number",
      placeholder: "Enter Your Number",
      labelname: "Enter Your Phone Number",
      validation: ["required", "phone"],
    },
    {
      type: "radio",
      name: "radio",
      id: "radio1",
      labelname: "Select for Yes",
      value: "yes",
      validation: ["required"],
    },
    {
      type: "radio",
      name: "radio",
      id: "radio-1",
      labelname: "Select for No",
      value: "no",
      validation: ["required"],
    },
    {
      type: "select",
      name: "select",
      id: "select",
      labelname: "Select Your Number",
      validation: ["required"],
      options: [
        { value: "1", text: "one" },
        { value: "2", text: "two" },
        { value: "3", text: "three" },
      ],
    },

    {
      type: "textarea",
      name: "textarea",
      id: "textarea",
      placeholder: "This is textarea",
      labelname: "Enter Your Skills",
      row: 2,
      validation: ["required", "min:5"],
    },
    {
      type: "checkbox",
      name: "checkbox",
      id: "checkbox",
      labelname: "Agree terms and condition",
      validation: ["required"],
    },
    {
      type: "submit",
      name: "submit",
      id: "submit",
    },
  ];

  callBack = (something) => {
    console.log(something);
  };
  render() {
    return (
      <div className="App">
        <h1>Custom Form</h1>
        <div className="form">
          <Form property={this.property} callback={this.callBack} />
        </div>
      </div>
    );
  }
}

export default App;
