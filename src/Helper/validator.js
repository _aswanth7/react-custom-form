import validator from "validator";

export const validatorFunc = (validationArray, value, type) => {
  let errors = [];
  validationArray.map((validation) => {
    if (validation === "required") {
      if (validator.isEmpty(value)) {
        errors.push(`${type} is required`);
      }
    } else if (validation === "email") {
      if (!validator.isEmail(value)) {
        errors.push(`Enter valid email`);
      }
    } else if (validation === "AlphaNumeric") {
      if (!validator.isAlphanumeric(value)) {
        errors.push(`Only AlphaNumeric allowed `);
      }
    } else if (validation === "password") {
      if (!validator.isStrongPassword(value)) {
        errors.push(`Password is not strong `);
      }
    } else if (/min:\d/.test(validation)) {
      let minLength = parseInt(validation.slice(4));

      if (value.length < minLength) {
        errors.push(`Minimum length is ${minLength} `);
      }
    } else if (/max:\d/.test(validation)) {
      let maxLength = parseInt(validation.slice(4));
      if (value.length > maxLength) {
        errors.push(`Maximum length is ${maxLength} `);
      }
    } else if (validation === "phone") {
      if (!validator.isMobilePhone(value, ["en-IN"])) {
        errors.push("Enter a valid phone number ");
      }
    } else if (validation === "currency") {
      if (!validator.isCurrency(value)) {
        errors.push("Enter a valid currency ");
      }
    } else {
      console.error("Not a valid validator");
    }
  });

  if (errors.length > 0) {
    return errors;
  } else {
    return null;
  }
};
